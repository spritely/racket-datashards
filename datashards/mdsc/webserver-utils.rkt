#lang racket

;; A bunch of these borrowed from Golem.  I guess I should library'ify
;; this.

(provide (all-defined-out))

(require json
         net/url
         html-writing
         racket/runtime-path
         web-server/http/response-structs)

;;; Utilities
;;; =========

;; current working directory... used for serving static content
(define-runtime-path cwd ".")

(define (ok body #:content-type [content-type #"application/octet-stream"])
  (response
   200 #"OK"
   (current-seconds) content-type
   '()
   (lambda (out-port)
     (write-bytes body out-port))))

(define (render-ok sxml)
  (ok (xexp->html-bytes sxml)
      #:content-type #"text/html"))

(define (not-found [body #"404 Not Found X_X"])
  (response 404 #"Not Found"
            (current-seconds) #"text/plain"
            '()
            (lambda (out-port)
              (display body out-port))))

(define (json->string json)
  (call-with-output-string
   (lambda (p)
     (write-json json p))))

(define (json->bytes json)
  (call-with-output-bytes
   (lambda (p)
     (write-json json p))))

(define (not-allowed)
  (response
   405 #"Method Not Allowed"
   (current-seconds) #"text/plain"
   '()
   (lambda (out-port)
     (write-bytes #"405 Method Not Allowed :P" out-port))))

(define (bad-request [body #"400 Bad Request"])
  (response
   400 #"Bad Request"
   (current-seconds) #"text/plain"
   '()
   (lambda (out-port)
     (write-bytes body out-port))))

(define (forbidden [body #"403 Forbidden"])
  (response
   403 #"Forbidden"
   (current-seconds) #"text/plain"
   '()
   (lambda (out-port)
     (write-bytes body out-port))))
