#lang racket

(require net/url)

(provide store<%>)

(define store<%>
  (interface ()
    [get (->m url? (or/c bytes? #f))]
    [put! (->m bytes? url?)]))
